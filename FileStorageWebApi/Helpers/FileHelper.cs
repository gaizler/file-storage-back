﻿using Microsoft.AspNetCore.StaticFiles;

namespace FileStorageWebApi.Helpers
{
    /// <summary>
    /// provides static methods to work with files
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// get file`s content type
        /// </summary>
        /// <param name="path">file`s path</param>
        /// <returns>content type</returns>
        public static string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();
            string contentType;

            if (!provider.TryGetContentType(path, out contentType))
            {
                contentType = "application/octet-stream";
            }

            return contentType;
        }
    }
}
