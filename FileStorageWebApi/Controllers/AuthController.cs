﻿using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Text.Json;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using FileStorageBLL.Exceptions;

namespace FileStorageWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authorizationService;
        public AuthController(IAuthService authorizationService)
        {
            _authorizationService = authorizationService;
        }

        // POST api/<AuthController>/signup
        [HttpPost("signup")]
        public async Task<ActionResult<string>> SignUp([FromBody] UserModel user)
        {
            if(!ModelState.IsValid)
                return BadRequest("Invalid model");

            try
            {
                var token = await _authorizationService.SignUpAsync(user);
                return JsonSerializer.Serialize(token);
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<AuthController>/signin
        [HttpPost("signin")]
        public async Task<ActionResult<string>> SignIn([FromBody] UserModel user)
        {
            try
            {
                var token = await _authorizationService.SignInAsync(user);
                return JsonSerializer.Serialize(token);
            }
            catch (FileStorageException)
            {
                return BadRequest("Wrong login or password");
            }
        }

        // GET api/<AuthController>/all
        [Authorize(Roles = "Admin")]
        [HttpGet("all")]
        public async Task<IEnumerable<UserModel>> GetAllRegistred()
        {
            return await _authorizationService.GetAllRegisteredAsync();
        }

        // GET api/<AuthController>/makeadmin/{username}
        [Authorize(Roles = "Admin")]
        [HttpGet("makeadmin/{username}")]
        public async Task<ActionResult> MakeAdmin(string username)
        {
            try
            {
                await _authorizationService.MakeAdminAsync(username);
                return Ok();
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
