﻿using FileStorageBLL.Exceptions;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageWebApi.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace FileStorageWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly IFileService _fileService;
        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        // GET: api/<FilesController>
        [Authorize(Roles ="Admin")]
        [HttpGet]
        public IEnumerable<FileModel> GetAll()
        {
            return _fileService.GetAll();
        }

        // GET api/<FilesController>/5
        [HttpGet("{id}")]
        public ActionResult<FileModel> GetById(int id)
        {
            try
            {
                var token = "";
                var a = Request.Headers["Authorization"].ToString();

                token = a.Replace("Bearer ", "");

                return _fileService.GetByIdWithAuthentification(id, token);
            }
            catch (FileStorageException)
            {
                return BadRequest("File not found");
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<FilesController>/username/all
        [Authorize]
        [HttpGet("{username}/all")]
        public IEnumerable<FileModel> GetFilesForUser(string username)
        {
            return _fileService.GetFilesForUser(username);
        }

        // GET: api/<FilesController>/search?request=somethingToSearch
        [Authorize(Roles = "Admin")]
        [HttpGet("search")]
        public ActionResult<IEnumerable<FileModel>> GetByRequest([FromQuery] string request)
        {
            IEnumerable<FileModel> files;
            try
            {
                files = _fileService.GetByTitle(request);

                if (files.Any())
                    return Ok(files);

                return BadRequest("Nothing found");
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
        }

        //DELETE api/<FilesController>/5
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _fileService.DeleteByIdAsync(id);
                return Ok();
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/<FilesController>/upload
        [Authorize]
        [HttpPost("upload"), DisableRequestSizeLimit]
        public async Task<IActionResult> Upload()
        {
            try
            {
                //saving file in file system
                var file = Request.Form.Files[0];
                var username = Request.Form["username"];
               
                var folderName = Path.Combine("Files", username);
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                _fileService.UploadFile(pathToSave, file);

                //adding record to db
                var model = new FileModel
                {
                    DateCreated = DateTime.Now,
                    FileUrl = Path.Combine(pathToSave,file.FileName),
                    IsPublic = Convert.ToBoolean(Request.Form["isPublic"]),
                    UserLogin = username,
                    Title = file.FileName
                };
                await _fileService.AddAsync(model);

                return Ok();
            }
            catch(FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/<FilesController>/download/{id}
        [HttpGet("download/{id}"), DisableRequestSizeLimit]
        public async Task<IActionResult> Download (int id)
        {
            try
            {
                var file = await _fileService.GetByIdAsync(id);
                if (file == null)
                    throw new FileStorageException("File not found");

                var memoryStream = await _fileService.DownloadAsync(file.FileUrl);
                return File(memoryStream, FileHelper.GetContentType(file.FileUrl), file.Title);
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }
    }
}