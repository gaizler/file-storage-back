﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FileStorageDAL.Migrations
{
    public partial class FileOwnerDeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_FilesOwners_FileOwnerId",
                table: "Files");

            migrationBuilder.DropTable(
                name: "FilesOwners");

            migrationBuilder.DropIndex(
                name: "IX_Files_FileOwnerId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "FileOwnerId",
                table: "Files");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Files",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Files_UserId",
                table: "Files",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_AspNetUsers_UserId",
                table: "Files",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_AspNetUsers_UserId",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_Files_UserId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Files");

            migrationBuilder.AddColumn<int>(
                name: "FileOwnerId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FilesOwners",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilesOwners", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "FilesOwners",
                columns: new[] { "Id", "Email", "Username" },
                values: new object[] { 1, "admin@gmail.com", "admin" });

            migrationBuilder.CreateIndex(
                name: "IX_Files_FileOwnerId",
                table: "Files",
                column: "FileOwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_FilesOwners_FileOwnerId",
                table: "Files",
                column: "FileOwnerId",
                principalTable: "FilesOwners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
