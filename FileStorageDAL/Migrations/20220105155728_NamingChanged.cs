﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FileStorageDAL.Migrations
{
    public partial class NamingChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_Clients_ClientId",
                table: "Files");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Files_ClientId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "ClientId",
                table: "Files");

            migrationBuilder.AddColumn<int>(
                name: "FileOwnerId",
                table: "Files",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FilesOwners",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilesOwners", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "FilesOwners",
                columns: new[] { "Id", "Email", "Username" },
                values: new object[] { 1, "admin@gmail.com", "admin" });

            migrationBuilder.CreateIndex(
                name: "IX_Files_FileOwnerId",
                table: "Files",
                column: "FileOwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_FilesOwners_FileOwnerId",
                table: "Files",
                column: "FileOwnerId",
                principalTable: "FilesOwners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Files_FilesOwners_FileOwnerId",
                table: "Files");

            migrationBuilder.DropTable(
                name: "FilesOwners");

            migrationBuilder.DropIndex(
                name: "IX_Files_FileOwnerId",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "FileOwnerId",
                table: "Files");

            migrationBuilder.AddColumn<int>(
                name: "ClientId",
                table: "Files",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Login = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Files_ClientId",
                table: "Files",
                column: "ClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Files_Clients_ClientId",
                table: "Files",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
