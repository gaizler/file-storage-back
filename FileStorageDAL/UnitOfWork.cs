﻿using FileStorageDAL.Interfaces;
using FileStorageDAL.Repositories;
using System.Threading.Tasks;

namespace FileStorageDAL
{
    // <inheritdoc/>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FileStorageDbContext _dbContext;
        private IFileRepository _fileRepository;

        public UnitOfWork(FileStorageDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IFileRepository FileRepository
        {
            get
            {
                if (_fileRepository == null)
                    _fileRepository = new FileRepository(_dbContext);
                return _fileRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
