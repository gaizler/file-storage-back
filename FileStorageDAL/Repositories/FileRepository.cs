﻿using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FileStorageDAL.Repositories
{
    public class FileRepository : IFileRepository
    {
        private readonly FileStorageDbContext _dbContext;
        public FileRepository(FileStorageDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(File entity)
        {
            await _dbContext.Files.AddAsync(entity);
        }

        public void Delete(File entity)
        {
            _dbContext.Files.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _dbContext.Files.FindAsync(id);

            if (entity != null)
            {
                _dbContext.Files.Remove(entity);
            }

        }

        public IQueryable<File> FindAll()
        {
            return _dbContext.Files.AsQueryable();
        }

        public IEnumerable<File> FindAll(Expression<Func<File, bool>> predicate)
        {
            return _dbContext.Files.Where(predicate);
        }

        public async Task<File> GetByIdAsync(int id)
        {
            return await _dbContext.Files.FindAsync(id);
        }

        public void Update(File entity)
        {
            _dbContext.Update(entity);
        }

        public IQueryable<File> Include(params Expression<Func<File, object>>[] includeProperties)
        {
            IQueryable<File> query = _dbContext.Files.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public IEnumerable<File> GetWithInclude(params Expression<Func<File, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }

        public IEnumerable<File> GetWithInclude(Func<File, bool> predicate,
                params Expression<Func<File, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Where(predicate).ToList();
        }
    }
}
