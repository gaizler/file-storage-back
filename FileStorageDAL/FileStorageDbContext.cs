﻿using FileStorageDAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace FileStorageDAL
{
    /// <summary>
    /// main db context of application,
    /// contains userfiles as well as authorization data
    /// </summary>
    public class FileStorageDbContext:IdentityDbContext<User,UserRole,Guid>
    {
        public FileStorageDbContext(DbContextOptions<FileStorageDbContext> options):base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<File> Files { get; set; }

    }
}
