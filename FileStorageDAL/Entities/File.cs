﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FileStorageDAL.Entities
{
    public class File:BaseEntity
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string FileUrl { get; set; }

        [Required]
        public DateTime DateOfLoad { get; set; }

        [Required]
        public bool IsPublic { get; set; }
        public virtual User User { get; set; }

    }
}
