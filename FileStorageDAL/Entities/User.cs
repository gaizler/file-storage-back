﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace FileStorageDAL.Entities
{
    public class User:IdentityUser<Guid>
    {
        public virtual ICollection<File> Files { get; set; }
    }
}
