﻿using FileStorageDAL.Entities;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// <inheritdoc/>,
    /// provides additional methods to work with FilesInDB (nothing here yet)
    /// </summary>
    public interface IFileRepository:IRepository<File>
    {
    }
}
