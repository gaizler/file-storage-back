﻿using System.Threading.Tasks;

namespace FileStorageDAL.Interfaces
{
    /// <summary>
    /// Unit of Work pattern implementation for current project
    /// </summary>
    public interface IUnitOfWork
    {
        IFileRepository FileRepository { get; }

        Task<int> SaveAsync();
    }
}
