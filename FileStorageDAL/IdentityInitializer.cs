﻿using FileStorageDAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace FileStorageDAL
{
    /// <summary>
    /// provides static method to initialize identity data
    /// </summary>
    public static class IdentityInitializer
    {
        /// <summary>
        /// initializes identity data, if it wasn`t initialized before
        /// </summary>
        public async static Task SeedData(UserManager<User> userManager, RoleManager<UserRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("RegisteredUser").Result)
            {
                UserRole role = new UserRole {Name="RegisteredUser" };
                await roleManager.CreateAsync(role);
            }

            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                UserRole role = new UserRole { Name="Admin"};
                await roleManager.CreateAsync(role);
            }

            if (userManager.FindByNameAsync("admin").Result == null)
            {
                User user = new User { UserName = "admin", Email = "admin@gmail.com" };

                var result = await userManager.CreateAsync(user, "admin2021");

                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, "Admin");
                }
            }
        }
    }
}
