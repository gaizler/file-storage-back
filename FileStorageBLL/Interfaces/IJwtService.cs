﻿using FileStorageDAL.Entities;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// provides methods to work with jwt
    /// </summary>
    public interface IJwtService
    {
        /// <summary>
        /// generate jwt token for user
        /// </summary>
        /// <param name="user">user, requested a token</param>
        /// <param name="roles">list of user roles</param>
        /// <returns>new jwt token</returns>
        string GenerateJwt(User user,IList<string> roles);

        /// <summary>
        /// decoding a jwt token
        /// </summary>
        /// <param name="token">stringyfied jwt token</param>
        /// <returns>decoded token</returns>
        JwtSecurityToken DecodeJwtToken(string token);
    }
}
