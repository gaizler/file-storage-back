﻿using FileStorageBLL.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// <inheritdoc/>,
    /// provides methods to work with FileModels
    /// </summary>
    public interface IFileService:ICrud<FileModel>
    {
        /// <summary>
        /// uploads file to file system
        /// </summary>
        /// <param name="pathToSave">path to save file in file system</param>
        /// <param name="file">exactly the file to save</param>
        void UploadFile(string pathToSave, IFormFile file);

        /// <summary>
        /// gets all files for requested user
        /// </summary>
        /// <param name="username">user`s username</param>
        /// <returns>collection of FileModels</returns>
        IEnumerable<FileModel> GetFilesForUser(string username);

        /// <summary>
        /// searchs file by id, executing necessary security aspects
        /// </summary>
        /// <param name="id">FileModel id</param>
        /// <param name="token">jwt token</param>
        /// <returns>FileModel by id</returns>
        FileModel GetByIdWithAuthentification(int id, string token);

        /// <summary>
        /// downloads file by id
        /// </summary>
        /// <param name="url">url of file in file system</param>
        /// <returns>memory stream, with file in it</returns>
        Task<MemoryStream> DownloadAsync(string url);

        /// <summary>
        /// searchs file by title
        /// </summary>
        /// <param name="request">user`s request to find</param>
        /// <returns>collection of searched FileModel</returns>
        IEnumerable<FileModel> GetByTitle(string request);
    }
}
