﻿using FileStorageBLL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// provides all necessary actions, related to users
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// register new user
        /// </summary>
        /// <param name="model">model with users login and password</param>
        /// <returns>jwt token</returns>
        Task<string> SignUpAsync(UserModel model);

        /// <summary>
        /// try to sign in user
        /// </summary>
        /// <param name="model">model with users login and password</param>
        /// <returns>jwt token</returns>
        Task<string> SignInAsync(UserModel model);

        /// <summary>
        /// gets all registered user
        /// </summary>
        /// <returns>collection of registered users</returns>
        Task<IEnumerable<UserModel>> GetAllRegisteredAsync();

        /// <summary>
        /// adds user to new role "Admin"
        /// </summary>
        /// <param name="username">user`s username</param>
        Task MakeAdminAsync(string username);
    }
}
