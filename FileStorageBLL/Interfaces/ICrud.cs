﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorageBLL.Interfaces
{
    /// <summary>
    /// provides typical methods, which execute CRUD operations
    /// </summary>
    public interface ICrud<TModel> where TModel : class
    {
        /// <summary>
        /// gets all models
        /// </summary>
        /// <returns>collection of all models</returns>
        IEnumerable<TModel> GetAll();

        /// <summary>
        /// gets model by id
        /// </summary>
        /// <param name="id">id of model in db</param>
        /// <returns>model, if it is found</returns>
        Task<TModel> GetByIdAsync(int id);

        /// <summary>
        /// adds a new model to db
        /// </summary>
        /// <param name="model">model to add</param>
        Task AddAsync(TModel model);

        /// <summary>
        /// updates a model in db
        /// </summary>
        /// <param name="model">model to update</param>
        Task UpdateAsync(TModel model);

        /// <summary>
        /// delete a model from db by id
        /// </summary>
        /// <param name="modelId">model id to delete</param>
        Task DeleteByIdAsync(int modelId);
    }
}
