﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace FileStorageBLL.Models
{
    /// <summary>
    /// DTO for user enity
    /// </summary>
    public class UserModel:IdentityUser<Guid>
    {
        public IList<string> Roles { get; set; }
        public string Password { get; set; }
        public ICollection<int> FilesIds { get; set; }
    }
}
