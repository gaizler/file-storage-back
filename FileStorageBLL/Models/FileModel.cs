﻿using System;

namespace FileStorageBLL.Models
{
    /// <summary>
    /// DTO for file entity
    /// </summary>
    public class FileModel
    {
        public int Id { get; set; }
        public string Title { get; set;}
        public string FileUrl { get; set; }
        public DateTime DateCreated { get; set; }   
        public string UserLogin { get; set; }
        public bool IsPublic { get; set; } 
    }
}
