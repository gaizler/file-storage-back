﻿using AutoMapper;
using FileStorageBLL.Exceptions;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorageBLL.Services
{
    
    // <inheritdoc/>
    public class AuthService:IAuthService
    {
        private readonly IJwtService _jwtService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public AuthService(UserManager<User> userManager,IMapper mapper, IJwtService jwtService)
        {
            _userManager = userManager;
            _mapper= mapper;
            _jwtService= jwtService;
        }

        public async Task<string> SignUpAsync(UserModel model)
        {
            var user = _mapper.Map<User>(model);
            var userCreateResult = await _userManager.CreateAsync(user,model.Password);

            if (!userCreateResult.Succeeded)
            {
                if(userCreateResult.Errors.Any())
                    throw new FileStorageException(userCreateResult.Errors.First().Description);
                throw new FileStorageException();
            }

            await _userManager.AddToRoleAsync(user, "RegisteredUser");
            var roles = await _userManager.GetRolesAsync(user);

            return _jwtService.GenerateJwt(user, roles);

        }

        public async Task<string> SignInAsync(UserModel model)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == model.UserName);

            if (user == null)
                throw new FileStorageException();

            var signInResult = await _userManager.CheckPasswordAsync(user,model.Password);
            if (!signInResult)
                throw new FileStorageException();

            var roles = await _userManager.GetRolesAsync(user);

            return _jwtService.GenerateJwt(user,roles);

        }

        public async Task<IEnumerable<UserModel>> GetAllRegisteredAsync()
        {
            var users = new List<UserModel>();

            foreach (var user in _userManager.Users.ToList())
            {
                var roles=await _userManager.GetRolesAsync(user);
                var userModel = _mapper.Map<UserModel>(user);
                userModel.Roles = roles;
                users.Add(userModel);
            }

            return users;
        }

        public async Task MakeAdminAsync(string username)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.UserName == username);

            if (user == null)
                throw new FileStorageException("User not found");

            await _userManager.AddToRoleAsync(user, "Admin");
        }
    }
}
