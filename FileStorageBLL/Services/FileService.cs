﻿using AutoMapper;
using FileStorageBLL.Exceptions;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageDAL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorageBLL.Services
{
    // <inheritdoc/>
    public class FileService : IFileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IJwtService _jwtService;
        private readonly UserManager<FileStorageDAL.Entities.User> _userManager;

        public FileService(IUnitOfWork unitOfWork,UserManager<FileStorageDAL.Entities.User> userManager, IMapper mapper, IJwtService jwtService)
        {
            _unitOfWork=unitOfWork;
            _userManager = userManager;
            _mapper=mapper; 
            _jwtService=jwtService;
        }

        public async Task AddAsync(FileModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var file = _mapper.Map<FileStorageDAL.Entities.File>(model);
            file.User = _userManager.Users.First(x => x.UserName == model.UserLogin);
            
            await _unitOfWork.FileRepository.AddAsync(file);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            //delete from file system
            var file = await _unitOfWork.FileRepository.GetByIdAsync(modelId);

            if (file == null)
                throw new FileStorageException("File not found");

            if (File.Exists(file.FileUrl))
            {
                File.Delete(file.FileUrl);
            }

            //delete from db
            await _unitOfWork.FileRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();

        }

        public IEnumerable<FileModel> GetAll()
        {
            return _mapper.Map<IEnumerable<FileModel>>(_unitOfWork.FileRepository.FindAll());
        }

        public async Task<FileModel> GetByIdAsync(int id)
        {
            return _mapper.Map<FileModel>(await _unitOfWork.FileRepository.GetByIdAsync(id));
        }

        public IEnumerable<FileModel> GetByTitle(string request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            return _mapper.Map<IEnumerable<FileModel>>(_unitOfWork.FileRepository.GetWithInclude(x => x.Title.ToLower().Contains(request.ToLower()), c => c.User));
        }

        public FileModel GetByIdWithAuthentification(int id, string token)
        {
            var file = _mapper.Map<FileModel>(_unitOfWork.FileRepository.GetWithInclude(x => x.Id == id, c => c.User).FirstOrDefault());

            if (file == null)
                throw new FileStorageException("File not found");

            if (!file.IsPublic)
            {
                //when user is unauthorized
                if (token == "Bearer")
                    throw new FileStorageException("File not found");

                var jwt = _jwtService.DecodeJwtToken(token);

                var login = jwt.Claims.FirstOrDefault(x => x.Type == "username").Value;
                var roles = jwt.Claims.FirstOrDefault(x => x.Type == "roles").Value;

                if (login == file.UserLogin || roles.Contains("Admin"))
                    return file;
                else
                    throw new FileStorageException("You don`t have access to this file");
            }
            else
                return file;
        }

        public async Task UpdateAsync(FileModel model)
        {
            if(model==null)
                throw new ArgumentNullException("model");

            var file =await _unitOfWork.FileRepository.GetByIdAsync(model.Id);

            if (file == null)
                throw new FileStorageException("File not found");

            file.Title = model.Title;
            file.DateOfLoad = model.DateCreated;

            _unitOfWork.FileRepository.Update(file);
            await _unitOfWork.SaveAsync();
        }

        public void UploadFile(string pathToSave, IFormFile file)
        {
            if (!Directory.Exists(pathToSave))
            {
                Directory.CreateDirectory(pathToSave);
            }

            if (file.Length > 0)
            {
                var fileName = file.FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);

                if (File.Exists(fullPath))
                    throw new FileStorageException("This file was already uploaded");

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
            else
            {
                throw new ArgumentException("Invalid file");
            }
        }

        public async Task<MemoryStream> DownloadAsync(string url)
        {
            if (!File.Exists(url))
                throw new FileStorageException("File not found");

            var memory = new MemoryStream();
            await using (var stream=new FileStream(url, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return memory;
        }

        public IEnumerable<FileModel> GetFilesForUser(string username)
        {
            return _mapper.Map<IEnumerable<FileModel>>(_unitOfWork.FileRepository.GetWithInclude(x=>x.User.UserName==username, c=>c.User));
        }

    }
}
