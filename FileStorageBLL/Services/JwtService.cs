﻿using FileStorageBLL.Interfaces;
using FileStorageBLL.Settings;
using FileStorageDAL.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace FileStorageBLL.Services
{
    // <inheritdoc/>
    public class JwtService : IJwtService
    {
        private readonly JwtSettingsModel _jwtSettings;
        public JwtService(IOptionsSnapshot<JwtSettingsModel> jwtSettings)
        {
            _jwtSettings=jwtSettings.Value;
        }

        public string GenerateJwt(User user, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim("username", user.UserName),
                new Claim("userId", user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("nameIdentifier", user.Id.ToString()),
                new Claim("roles",String.Join(", ",roles))
            };

            var roleClaims = roles.Select(r => new Claim(ClaimTypes.Role, r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public JwtSecurityToken DecodeJwtToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            if(handler.CanReadToken(token))
                return handler.ReadJwtToken(token);
            throw new ArgumentException("Token is not valid");
        }
    }
}
