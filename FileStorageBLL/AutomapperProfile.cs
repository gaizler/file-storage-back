﻿using AutoMapper;
using FileStorageBLL.Models;
using FileStorageDAL.Entities;
using System.Linq;

namespace FileStorageBLL
{
    /// <summary>
    /// automapper profile, which provides map from FileStorageEntities to FileStorageModels
    /// and reverse maps to them
    /// </summary>
    public class AutomapperProfile:Profile
    {
        public AutomapperProfile()
        {

            CreateMap<File, FileModel>()
                .ForMember(x => x.DateCreated, c => c.MapFrom(z => z.DateOfLoad))
                .ForMember(x => x.UserLogin, c => c.MapFrom(z => z.User.UserName));

            CreateMap<FileModel, File>()
                .ForMember(x => x.DateOfLoad, c => c.MapFrom(z => z.DateCreated));

            CreateMap<User, UserModel>()
                .ForMember(x => x.FilesIds, c => c.MapFrom(z => z.Files.Select(x => x.Id)))
                .ReverseMap();

        }
    }
}
