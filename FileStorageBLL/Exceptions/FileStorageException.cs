﻿using System;
using System.Runtime.Serialization;

namespace FileStorageBLL.Exceptions
{
    [Serializable]
    public class FileStorageException:Exception
    {
        public FileStorageException(){ }        
        public FileStorageException(string message):base(message){}
        public FileStorageException(string msg, Exception inner) : base(msg, inner) { }
        protected FileStorageException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
