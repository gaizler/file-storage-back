﻿namespace FileStorageBLL.Settings
{
    public class JwtSettingsModel
    {
        public string Issuer { get; set; }
        public string Secret { get; set; }
        public int ExpirationInDays { get; set; }
    }
}
