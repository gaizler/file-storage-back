﻿using System.Linq;
using System.Threading.Tasks;
using FileStorageDAL;
using FileStorageDAL.Entities;
using FileStorageDAL.Repositories;
using NUnit.Framework;

namespace FileStorageTests.TestsDAL
{
    /// <summary>
    /// unit tests for FileRepository
    /// </summary>
    public class FileRepositoryTests
    {
        [Test]
        public void FindAll_ValidCall()
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var files = fileRepo.FindAll().ToList();

            //assert
            Assert.AreEqual(3, files.Count);
        }

        [TestCase(1, "title1")]
        [TestCase(2, "title2")]
        public async Task GetById_ValidCall(int id, string title)
        {
            //arange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var file = await fileRepo.GetByIdAsync(id);

            //assert
            Assert.AreEqual(id, file.Id);
            Assert.AreEqual(title, file.Title);
        }

        [TestCase(100)]
        public async Task GetById_ReturnsNull(int id)
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var file = await fileRepo.GetByIdAsync(id);

            //assert
            Assert.IsNull(file);
        }

        [TestCase(10)]
        public async Task AddAsync_ValidCall(int id)
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);
            var expected = context.Files.Count() + 1;

            //act
            await fileRepo.AddAsync(new File { Id = id });
            await context.SaveChangesAsync();
            var actual = context.Files.Count();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(3)]
        public async Task DeleteByIdAsync_ValidCall(int id)
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);
            var expected = context.Files.Count() - 1;

            //act
            await fileRepo.DeleteByIdAsync(id);
            await context.SaveChangesAsync();
            var actual = context.Files.Count();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(3)]
        public async Task Delete_ValidCall(int id)
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);
            var expected = context.Files.Count() - 1;

            //act
            var file = context.Files.First(x => x.Id == id);
            fileRepo.Delete(file);
            await context.SaveChangesAsync();
            var actual = context.Files.Count();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(3)]
        public async Task UpdateAsync_ValidCall(int id)
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var file = context.Files.First(x => x.Id == id);
            file.Title = "updatedTitle";
            fileRepo.Update(file);
            await context.SaveChangesAsync();

            //assert
            var newFile = context.Files.First(x => x.Id == id);
            Assert.AreEqual(file.Title, newFile.Title);
        }

        [Test]
        public void GetWithInclude_ValidCall()
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var files = fileRepo.GetWithInclude(x => x.User).ToList();
            var actual = files.First(x => x.Title == "title1").User.UserName;

            //assert
            Assert.AreEqual("registered", actual);
        }

        [TestCase("title1", "registered")]
        [TestCase("title3", "registered")]
        public void GetWithIncludeWithPredicate_ValidCall(string title, string username)
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var file = fileRepo.GetWithInclude(x => x.Title == title, i => i.User).First();

            //assert
            Assert.AreEqual(username, file.User.UserName);
        }

        [Test]
        public void Include_ValidCall()
        {
            //arrange
            using var context = new FileStorageDbContext(UnitTestHelper.GetUnitTestDbOptions());
            var fileRepo = new FileRepository(context);

            //act
            var files = fileRepo.Include(x => x.User).ToList();
            var fileToTest = files.First(x => x.Title == "title3");

            //assert
            Assert.AreEqual("registered", fileToTest.User.UserName);
        }
    }
}
