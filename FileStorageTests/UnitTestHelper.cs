﻿using AutoMapper;
using FileStorageBLL;
using FileStorageBLL.Models;
using FileStorageDAL;
using FileStorageDAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Text;

namespace FileStorageTests
{
    /// <summary>
    /// provide static methods, which were used during writing unit tests
    /// </summary>
    public static class UnitTestHelper
    {
        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }

        public static IEnumerable<FileModel> GetTestFileModels()
        {
            return new List<FileModel>
            {
                new FileModel
                {
                    Id=1,
                    Title="Title1",
                    DateCreated=new DateTime(2021,12,12),
                    UserLogin="user1",
                    FileUrl="url1",
                    IsPublic=true
                },
                new FileModel
                {
                    Id=2,
                    Title="Title2",
                    DateCreated=new DateTime(2021,12,12),
                    UserLogin="user1",
                    FileUrl="url2",
                    IsPublic=false
                },
                new FileModel
                {
                    Id=3,
                    Title="Title3",
                    DateCreated=new DateTime(2021,12,12),
                    UserLogin="user2",
                    FileUrl="url3",
                    IsPublic=true
                },
                new FileModel
                {
                    Id=4,
                    Title="Title4",
                    DateCreated=new DateTime(2021,12,12),
                    UserLogin="user3",
                    FileUrl="url4",
                    IsPublic=true
                },
                new FileModel
                {
                    Id=5,
                    Title="Title5",
                    DateCreated=new DateTime(2021,12,12),
                    UserLogin="user3",
                    FileUrl="url5",
                    IsPublic=false
                }
            };
        }

        public static IEnumerable<File> GetTestFileEntities()
        {
            return new List<File>
            {
                new File
                {
                    Id=1,
                    Title="Title1",
                    DateOfLoad=new DateTime(2021,12,12),
                    User=new User{UserName="user1"},
                    FileUrl="url1",
                    IsPublic=true
                },
                new File
                {
                    Id=2,
                    Title="Title2",
                    DateOfLoad=new DateTime(2021,12,12),
                    User=new User{UserName="user1"},
                    FileUrl="url2",
                    IsPublic=false
                },
                new File
                {
                    Id=3,
                    Title="Title3",
                    DateOfLoad=new DateTime(2021,12,12),
                    User=new User{UserName="user2"},
                    FileUrl="url3",
                    IsPublic=true
                },
                new File
                {
                    Id=4,
                    Title="Title4",
                    DateOfLoad=new DateTime(2021,12,12),
                    User=new User{UserName="user3"},
                    FileUrl="url4",
                    IsPublic=true
                },
                new File
                {
                    Id=5,
                    Title="Title5",
                    DateOfLoad=new DateTime(2021,12,12),
                    User=new User{UserName="user3"},
                    FileUrl="url5",
                    IsPublic=false
                }
            };

        }

        public static IEnumerable<UserModel> GetTestUserModels()
        {
            return new List<UserModel>
            {
                new UserModel
                {
                    UserName="user1",
                    Password="password1",
                    Roles= new List<string> { "Registered" }
                },
                new UserModel
                {
                    UserName="user2",
                    Password="password2",
                    Roles= new List<string> { "Registered" }
                },
                new UserModel
                {
                    UserName="user3",
                    Password="password3",
                    Roles= new List<string> { "Registered" }
                },
                new UserModel
                {
                    UserName="user4",
                    Password="password4",
                    Roles= new List<string> { "Registered" }
                },
                new UserModel
                {
                    UserName="user5",
                    Password="password5",
                    Roles= new List<string> { "Registered" }
                }
            };

        }

        public static IEnumerable<User> GetTestUserEntities()
        {
            return new List<User>
            {
                new User
                {
                    UserName="user1",
                },
                new User
                {
                    UserName="user2"
                },
                new User
                {
                    UserName="user3"
                },
                new User
                {
                    UserName="user4"
                },
                new User
                {
                    UserName="user5"
                },
            };

        }

        public static JwtSecurityToken CreateSimpleToken()
        {
            var token = new JwtSecurityToken();
            token.Payload.TryAdd("username", "user1");
            token.Payload.TryAdd("roles", "");

            return token;
        }

        public static JwtSecurityToken CreateAdminToken()
        {
            var token = new JwtSecurityToken();
            token.Payload.TryAdd("username", "user1");
            token.Payload.TryAdd("roles", "Admin");

            return token;
        }

        public static DbContextOptions<FileStorageDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<FileStorageDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new FileStorageDbContext(options))
            {
                SeedData(context);
            }

            return options;
        }

        public static void SeedData(FileStorageDbContext context)
        {
            using var sha256 = SHA256.Create();
            var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes("qwerty123"));
            string passHash1 = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

            var role1 = new UserRole { Name = "Registered" };
            var role2 = new UserRole { Name = "Admin" };
            var user1 = new User { UserName = "registered",PasswordHash=passHash1, SecurityStamp="securityStamp" };

            var file1 = new File { Id = 1, User=user1, DateOfLoad = DateTime.Now, IsPublic = false, Title = "title1", FileUrl = "url1" };
            var file2 = new File { Id = 2, User=user1, DateOfLoad = DateTime.Now, IsPublic = true, Title = "title2", FileUrl = "url2" };
            var file3 = new File { Id = 3, User=user1, DateOfLoad = DateTime.Now, IsPublic = false, Title = "title3", FileUrl = @"D:\Programming\.NetSummerProgram2021\FinalTask\FileStorage\FileStorageTests\SampleFiles\AbstractFactory.pdf" };

            context.Files.AddRange(file1,file2,file3);
            context.Users.Add(user1);
            context.Roles.AddRange(role1, role2);
            context.UserRoles.Add(new IdentityUserRole<Guid> { RoleId = role1.Id, UserId = user1.Id });

            context.SaveChanges();
        }

        public static JwtSecurityToken DecodeJwtToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            if (handler.CanReadToken(token))
                return handler.ReadJwtToken(token);
            throw new ArgumentException("Token is not valid");
        }
    }
}
