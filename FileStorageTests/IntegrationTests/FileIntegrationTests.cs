﻿using FileStorageBLL.Models;
using FileStorageDAL;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FileStorageTests.IntegrationTests
{
    /// <summary>
    /// integration tests for FileController
    /// </summary>
    internal class FileIntegrationTests
    {
        private HttpClient _client;
        private CustomWebApplicationFactory _factory;
        private const string _requestUri = "api/files";

        [SetUp]
        public void SetUp()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Test]
        public async Task GetAll_ValidCall()
        {
            //arrange
            var expectedCount = 3;
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri);
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse=await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            //assert
            var files = JsonConvert.DeserializeObject<IEnumerable<FileModel>>(stringResponse);
            Assert.AreEqual(expectedCount, files.Count());
        }

        [TestCase(1)]
        public async Task GetById_ValidCall(int id)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/{id}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
           
            //act
            var httpResponse = await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            //assert
            var file = JsonConvert.DeserializeObject<FileModel>(stringResponse);
            Assert.AreEqual(id, file.Id);
        }

        [TestCase(100)]
        public async Task GetById_ReturnsBadRequest(int id)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/{id}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
           
            //act
            var httpResponse = await _client.SendAsync(request);
            
            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [TestCase("registered",3)]
        [TestCase("admin", 0)]
        [TestCase("enexisting",0)]
        public async Task GetFileForUser_ValidCall(string username,int count)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/{username}/all");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
           
            //act
            var httpResponse = await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var files = JsonConvert.DeserializeObject<IEnumerable<FileModel>>(stringResponse);

            //assert
            Assert.AreEqual(count,files.Count());
            
        }

        [TestCase("title", 3)]
        [TestCase("T", 3)]
        [TestCase("TITLE1", 1)]
        public async Task GetByRequest_ValidCall(string req, int expextedCount)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/search?request={req}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();

            //assert
            var files = JsonConvert.DeserializeObject<IEnumerable<FileModel>>(stringResponse);
            Assert.AreEqual(expextedCount, files.Count());
        }

        [TestCase("something_absent")]
        [TestCase("")]
        [TestCase(null)]
        public async Task GetByRequest_ReturnsBadRequest(string req)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/search?request={req}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);
            
            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [TestCase(1)]
        public async Task DeleteFile_ValidCall(int id,int expectedCount=2)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Delete, _requestUri + $"/{id}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();

            //assert
            using var test = _factory.Services.CreateScope();
            var context=test.ServiceProvider.GetService<FileStorageDbContext>();
            var actualCount = context.Files.Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestCase(100)]
        public async Task DeleteFile_ReturnsBadRequest(int id)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Delete, _requestUri + $"/{id}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);

            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [TestCase(3)]
        public async Task DownloadFile_ValidCall(int id)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/download/{id}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);
            
            //assert
            Assert.That(httpResponse.IsSuccessStatusCode);
        }

        [TestCase(100)]
        public async Task DownloadFile_ReturnsBadRequest(int id)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/download/{id}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
           
            //act
            var httpResponse = await _client.SendAsync(request);

            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }
    }
}
