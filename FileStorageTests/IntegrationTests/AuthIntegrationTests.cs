﻿using FileStorageBLL.Models;
using FileStorageDAL;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FileStorageTests.IntegrationTests
{
    /// <summary>
    /// integration tests for AuthControlles
    /// </summary>
    internal class AuthIntegrationTests
    {
        private HttpClient _client;
        private CustomWebApplicationFactory _factory;
        private const string _requestUri = "api/auth";

        [SetUp]
        public void SetUp()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Test]
        public async Task SignIn_ValidCall()
        {
            //arrange
            var expected = "admin";
            var user = new UserModel { UserName = "admin", Password = "admin2021" };
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");

            //act
            var httpResponse = await _client.PostAsync(_requestUri+"/signin", content);
            httpResponse.EnsureSuccessStatusCode();

            //assert
            var tokenJson = await httpResponse.Content.ReadAsStringAsync();
            var tokenStr =JsonConvert.DeserializeObject<string>(tokenJson);
            var token = UnitTestHelper.DecodeJwtToken(tokenStr);
            var actual = token.Claims.First(x => x.Type == "username").Value;

            Assert.AreEqual(expected,actual);
        }

        [TestCase("admin","wrongPsw")]
        [TestCase("wrongUser", "wrongPsw")]
        [TestCase("wrongUser", "admin2021")]
        [TestCase("", "")]
        public async Task SignIn_ReturnsBadRequest(string username,string password)
        {
            //arrange
            var user = new UserModel { UserName = username, Password =password };
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            
            //act
            var httpResponse = await _client.PostAsync(_requestUri + "/signin", content);
            
            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task SignUp_ValidCall()
        {
            //arrange
            var expectedCount = 3;
            var user = new UserModel { UserName = "newUser", Password = "newUserPsw" };
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");

            //act
            var httpResponse = await _client.PostAsync(_requestUri + "/signup", content);
            httpResponse.EnsureSuccessStatusCode();

            //assert
            using var test = _factory.Services.CreateScope();
            var context = test.ServiceProvider.GetService<FileStorageDbContext>();
            var actualCount = context.Users.Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestCase("admin","somePassword")]
        [TestCase("", "")]
        [TestCase("newUser", "123")]
        public async Task SignUp_ReturnsBadRequest(string username,string password)
        {
            //arrange
            var user = new UserModel { UserName = username, Password = password };
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            
            //act
            var httpResponse = await _client.PostAsync(_requestUri + "/signup", content);

            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task GetAllRegistered_ValidCall()
        {
            //arrange
            var expectedCount = 2;
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + "/all");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();

            //assert
            using var test = _factory.Services.CreateScope();
            var context = test.ServiceProvider.GetService<FileStorageDbContext>();
            var actualCount = context.Users.Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [Test]
        public async Task GetAllRegistered_RenurnsUnauthorized()
        {
            //act
            var httpResponse = await _client.GetAsync(_requestUri + "/all");

            //assert
            Assert.That(httpResponse.StatusCode == HttpStatusCode.Unauthorized);
        }

        [TestCase("registered")]
        public async Task MakeAdmin_ValidCall(string username)
        {
            //arrange
            var expectedCount = 3;
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/makeadmin/{username}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
            
            //act
            var httpResponse = await _client.SendAsync(request);
            httpResponse.EnsureSuccessStatusCode();

            //assert
            using var test = _factory.Services.CreateScope();
            var context = test.ServiceProvider.GetService<FileStorageDbContext>();
            var actualCount = context.UserRoles.Count();
            Assert.AreEqual(expectedCount,actualCount);
        }

        [TestCase("unexisting")]
        public async Task MakeAdmin_ReturnsBadRequest(string username)
        {
            //arrange
            using var request = new HttpRequestMessage(HttpMethod.Get, _requestUri + $"/makeadmin/{username}");
            request.Headers.Authorization =
                new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiNzMyNmIyOWYtODgwOS00OTdkLTVlOGEtMDhkOWQ0ZTE5ZTY2IiwianRpIjoiNzE0OTA5MjUtZGI4MS00Nzk4LWI1NmMtNmI0NWI0OTAxNDVmIiwibmFtZUlkZW50aWZpZXIiOiI3MzI2YjI5Zi04ODA5LTQ5N2QtNWU4YS0wOGQ5ZDRlMTllNjYiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ1NzQ4MzYsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.DkEKfUJjAY5Frf2I9cDv0HjuZcKAHarZHg-5IgMhXoE");
           
            //act
            var httpResponse = await _client.SendAsync(request);
            
            //assert
            Assert.That(httpResponse.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }

    }
}
