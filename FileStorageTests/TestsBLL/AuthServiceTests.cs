﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileStorageBLL.Exceptions;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageBLL.Services;
using FileStorageDAL.Entities;
using Microsoft.AspNetCore.Identity;
using Moq;
using NUnit.Framework;

namespace FileStorageTests.TestBLL
{
    /// <summary>
    /// unit tests for AuthService
    /// </summary>
    public class AuthServiceTests
    {
        private Mock<IJwtService> _mockJwtService;
        private Mock<UserManager<User>> _mockUserManager;

        [SetUp]
        public void Setup()
        {
            _mockJwtService = new Mock<IJwtService>();
            _mockUserManager = new Mock<UserManager<User>>(Mock.Of<IUserStore<User>>(), null, null, null, null, null, null, null, null);
        }

        [Test]
        public async Task SignUp_ValidCall()
        {
            //arrange
            _mockUserManager.Setup(m => m.CreateAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));
            _mockUserManager.Setup(m=>m.AddToRoleAsync(It.IsAny<User>(),It.IsAny<string>()));
            _mockUserManager.Setup(m => m.GetRolesAsync(It.IsAny<User>()));
            _mockJwtService.Setup(m=>m.GenerateJwt(It.IsAny<User>(),It.IsAny<IList<string>>()));

            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);
            
            //act
            await authService.SignUpAsync(UnitTestHelper.GetTestUserModels().First());

            //assert
            _mockUserManager.Verify(x=>x.CreateAsync(It.IsAny<User>(), It.IsAny<string>()),Times.Exactly(1));
            _mockUserManager.Verify(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()), Times.Exactly(1));
            _mockUserManager.Verify(x => x.GetRolesAsync(It.IsAny<User>()), Times.Exactly(1));
            _mockJwtService.Verify(x => x.GenerateJwt(It.IsAny<User>(), It.IsAny<IList<string>>()), Times.Exactly(1));
        }

        [Test]
        public void SignUp_ThrowsException()
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUserManager.Setup(m => m.CreateAsync(It.IsAny<User>(),It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Failed()));

            var authService = new AuthService(_mockUserManager.Object,UnitTestHelper.CreateMapperProfile(),_mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(()=>authService.SignUpAsync(UnitTestHelper.GetTestUserModels().First()));
            
            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [TestCase("user_which_dont_exist")]
        [TestCase("")]
        [TestCase(null)]
        public void SignIn_ThrowsException_WrongLogin(string username)
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUserManager.SetupGet(p => p.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual=Assert.CatchAsync(() => authService.SignInAsync(new UserModel {UserName=username}));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }


        [TestCase("user1","wrong_psw")]
        public void SignIn_ThrowsException_WrongPassword(string username,string password)
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUserManager.SetupGet(p => p.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            _mockUserManager.Setup(m => m.CheckPasswordAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));

            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(() => authService.SignInAsync(new UserModel { UserName = username,Password=password }));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [TestCase("user1", "correct_psw")]
        public async Task SignIn_ValidCall(string username, string password)
        {
            //arrange
            _mockUserManager.SetupGet(p => p.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            _mockUserManager.Setup(m => m.CheckPasswordAsync(It.IsAny<User>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            _mockUserManager.Setup(m=>m.GetRolesAsync(It.IsAny<User>()));
            _mockJwtService.Setup(m => m.GenerateJwt(It.IsAny<User>(),It.IsAny<IList<string>>()));

            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            await authService.SignInAsync(new UserModel { UserName = username, Password = password });

            //assert
            _mockUserManager.Verify(m => m.GetRolesAsync(It.IsAny<User>()), Times.Exactly(1));
            _mockJwtService.Verify(m=>m.GenerateJwt(It.IsAny<User>(), It.IsAny<IList<string>>()),Times.Exactly(1));

        }

        [TestCase("someone")]
        [TestCase("")]
        [TestCase(null)]
        public void MakeAdmin_ThrowsException(string username)
        {
            //arrange
            var expected = typeof(FileStorageException);

            _mockUserManager.SetupGet(x => x.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(() => authService.MakeAdminAsync(username));
            
            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [TestCase("user1")]
        public async Task MakeAdmin_ValidCall(string username)
        {
            //arrange
            _mockUserManager.SetupGet(x => x.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            _mockUserManager.Setup(x => x.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()));
            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            await authService.MakeAdminAsync(username);

            //assert
            _mockUserManager.Verify(m => m.AddToRoleAsync(It.IsAny<User>(), It.IsAny<string>()),Times.Exactly(1));
        }

        [Test]
        public async Task GetAll_ValidCall()
        {
            //arrange
            var expected = UnitTestHelper.GetTestUserModels();

            _mockUserManager.SetupGet(p => p.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            _mockUserManager.Setup(m => m.GetRolesAsync(It.IsAny<User>()));
            var authService = new AuthService(_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);
            
            //act
            var actual = await authService.GetAllRegisteredAsync();

            //assert
            Assert.AreEqual(expected.Count(),actual.Count());
            for(int i = 0; i < expected.Count(); i++)
            {
                Assert.AreEqual(expected.ToList()[i].UserName, actual.ToList()[i].UserName);
            }
            _mockUserManager.Verify(m => m.GetRolesAsync(It.IsAny<User>()), Times.Exactly(actual.Count()));


        }
    }
}
