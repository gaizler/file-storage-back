﻿using FileStorageBLL.Exceptions;
using FileStorageBLL.Interfaces;
using FileStorageBLL.Models;
using FileStorageBLL.Services;
using FileStorageDAL.Entities;
using FileStorageDAL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FileStorageTests.TestBLL
{
    /// <summary>
    /// unit tests for file service
    /// </summary>
    public class FileServiceTests
    {
        private readonly string _sampleFilesUrl = @"D:\Programming\.NetSummerProgram2021\FinalTask\FileStorage\FileStorageTests\SampleFiles";
        private readonly string _existingFileUrl = @"D:\Programming\.NetSummerProgram2021\FinalTask\FileStorage\FileStorageTests\SampleFiles\AbstractFactory.pdf";
        private readonly string _notUploadedFileUrl = @"D:\Programming\.NetSummerProgram2021\FinalTask\ExampleFiles\Proxy.pdf";
        private readonly string _unexistingFileUrl = @"D:\Programming\.NetSummerProgram2021\FinalTask\FileStorage\FileStorageTests\SampleFiles\unexistingFile.pdf";


        private Mock<IUnitOfWork> _mockUnitOfWork;
        private Mock<IJwtService> _mockJwtService;
        private Mock<UserManager<User>> _mockUserManager;


        [SetUp]
        public void Setup()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockJwtService = new Mock<IJwtService>();
            _mockUserManager = new Mock<UserManager<User>>(Mock.Of<IUserStore<User>>(), null, null, null, null, null, null, null, null);
        }

        [TestCase("user1")]
        [TestCase("user2")]
        [TestCase("user99")]

        public void GetFilesForUser_ValidCall(string username)
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().Where(x => x.UserLogin == username).ToList();

            _mockUnitOfWork
                .Setup(m => m.FileRepository.GetWithInclude(
                    It.IsAny<Func<File, bool>>(),
                    It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => x.User.UserName == username));
            var fileService = new FileService(_mockUnitOfWork.Object,_mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = fileService.GetFilesForUser(username).ToList();

            //assert
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i].Id, actual[i].Id);
                Assert.AreEqual(expected[i].Title, actual[i].Title);
                Assert.AreEqual(expected[i].IsPublic, actual[i].IsPublic);
                Assert.AreEqual(expected[i].UserLogin, actual[i].UserLogin);
                Assert.AreEqual(expected[i].DateCreated, actual[i].DateCreated);
                Assert.AreEqual(expected[i].FileUrl, actual[i].FileUrl);
            }
        }

        [TestCase("title")]
        [TestCase("something_not_existing")]
        public void GetByTitle_ValidCall(string request)
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().Where(x => x.Title.ToLower().Contains(request.ToLower())).ToList();
            _mockUnitOfWork
                .Setup(m => m.FileRepository.GetWithInclude(
                    It.IsAny<Func<File, bool>>(),
                    It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => x.Title.ToLower().Contains(request.ToLower())));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = fileService.GetByTitle(request).ToList();

            //assert
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i].Id, actual[i].Id);
                Assert.AreEqual(expected[i].Title, actual[i].Title);
                Assert.AreEqual(expected[i].IsPublic, actual[i].IsPublic);
                Assert.AreEqual(expected[i].UserLogin, actual[i].UserLogin);
                Assert.AreEqual(expected[i].DateCreated, actual[i].DateCreated);
                Assert.AreEqual(expected[i].FileUrl, actual[i].FileUrl);
            }
        }

        [TestCase(null)]
        public void GetByTitle_ThrowsArgumentNullException(string request)
        {
            //arrange
            var expected = typeof(ArgumentNullException);
            _mockUnitOfWork
                .Setup(m => m.FileRepository.GetWithInclude(
                    It.IsAny<Func<File, bool>>(),
                    It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => x.Title.ToLower().Contains(request.ToLower())));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.Catch(() => fileService.GetByTitle(request));

            //assert
            Assert.AreEqual(expected, actual.GetType());

        }

        [Test]
        public void GetAll_ValidCall()
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().ToList();
            _mockUnitOfWork.Setup(m => m.FileRepository.FindAll())
                .Returns(UnitTestHelper.GetTestFileEntities().AsQueryable());
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = fileService.GetAll().ToList();

            //assert
            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i].Id, actual[i].Id);
                Assert.AreEqual(expected[i].Title, actual[i].Title);
                Assert.AreEqual(expected[i].IsPublic, actual[i].IsPublic);
                Assert.AreEqual(expected[i].UserLogin, actual[i].UserLogin);
                Assert.AreEqual(expected[i].DateCreated, actual[i].DateCreated);
                Assert.AreEqual(expected[i].FileUrl, actual[i].FileUrl);
            }
        }

        [TestCase(1)]
        public async Task GetById_ValidCall(int id)
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().FirstOrDefault(x => x.Id == id);
            _mockUnitOfWork.Setup(m => m.FileRepository.GetByIdAsync(id))
                .Returns(Task.FromResult(UnitTestHelper.GetTestFileEntities().FirstOrDefault(x => x.Id == id)));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = await fileService.GetByIdAsync(id);


            //assert
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.IsPublic, actual.IsPublic);
            Assert.AreEqual(expected.UserLogin, actual.UserLogin);
            Assert.AreEqual(expected.DateCreated, actual.DateCreated);
            Assert.AreEqual(expected.FileUrl, actual.FileUrl);
        }

        [TestCase(12)]
        [TestCase(-12)]
        public async Task GetById_ReturnsNull(int id)
        {
            //arrange
            _mockUnitOfWork.Setup(m => m.FileRepository.GetByIdAsync(id))
                .Returns(Task.FromResult(UnitTestHelper.GetTestFileEntities().FirstOrDefault(x => x.Id == id)));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = await fileService.GetByIdAsync(id);

            //assert
            Assert.IsNull(actual);
        }

        [Test]
        public void GetByIdWithAuthentification_ThrowsException_NotFound()
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUnitOfWork.Setup(m => m.FileRepository.GetWithInclude(
                It.IsAny<Func<File, bool>>(),
                It.IsAny<Expression<Func<File, object>>>()));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.Catch(() => fileService.GetByIdWithAuthentification(UnitTestHelper.GetTestFileModels().First().Id, "token"));

            //assert
            Assert.AreEqual(expected, actual.GetType());

        }

        [Test]
        public void GetByIdWithAuthentification_ThrowsException_PrivateFile()
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUnitOfWork.Setup(m => m.FileRepository.GetWithInclude(
                It.IsAny<Func<File, bool>>(),
                It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => !x.IsPublic));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.Catch(() => fileService.GetByIdWithAuthentification(UnitTestHelper.GetTestFileModels().First().Id, "Bearer"));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [Test]
        public void GetByIdWithAuthentification_ThrowsException_PrivateFile2()
        {
            //arrange
            var expected = typeof(FileStorageException);

            _mockUnitOfWork.Setup(m => m.FileRepository.GetWithInclude(
                It.IsAny<Func<File, bool>>(),
                It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => !x.IsPublic && x.User.UserName != "user1"));

            _mockJwtService.Setup(m => m.DecodeJwtToken(It.IsAny<string>()))
                .Returns(UnitTestHelper.CreateSimpleToken());

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.Catch(() => fileService.GetByIdWithAuthentification(UnitTestHelper.GetTestFileModels().First().Id, "Bearer + some token"));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [Test]
        public void GetByIdWithAuthentification_ValidCall_PublicFile()
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().First(x => x.IsPublic);

            _mockUnitOfWork.Setup(m => m.FileRepository.GetWithInclude(
                It.IsAny<Func<File, bool>>(),
                It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => x.IsPublic));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = fileService.GetByIdWithAuthentification(1, "Bearer");

            //assert
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.IsPublic, actual.IsPublic);
            Assert.AreEqual(expected.UserLogin, actual.UserLogin);
            Assert.AreEqual(expected.DateCreated, actual.DateCreated);
            Assert.AreEqual(expected.FileUrl, actual.FileUrl);
        }

        [Test]
        public void GetByIdWithAuthentification_ValidCall_Owner()
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().First(x => x.UserLogin == "user1" && !x.IsPublic);

            _mockUnitOfWork.Setup(m => m.FileRepository.GetWithInclude(
                It.IsAny<Func<File, bool>>(),
                It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => !x.IsPublic && x.User.UserName == "user1"));

            _mockJwtService.Setup(m => m.DecodeJwtToken(It.IsAny<string>()))
              .Returns(UnitTestHelper.CreateSimpleToken());

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = fileService.GetByIdWithAuthentification(1, "Bearer + some token");

            //assert
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.IsPublic, actual.IsPublic);
            Assert.AreEqual(expected.UserLogin, actual.UserLogin);
            Assert.AreEqual(expected.DateCreated, actual.DateCreated);
            Assert.AreEqual(expected.FileUrl, actual.FileUrl);
        }

        [Test]
        public void GetByIdWithAuthentification_ValidCall_Admin()
        {
            //arrange
            var expected = UnitTestHelper.GetTestFileModels().First(x => x.UserLogin != "user1" && !x.IsPublic);

            _mockUnitOfWork.Setup(m => m.FileRepository.GetWithInclude(
                It.IsAny<Func<File, bool>>(),
                It.IsAny<Expression<Func<File, object>>>()))
                .Returns(UnitTestHelper.GetTestFileEntities().Where(x => !x.IsPublic && x.User.UserName != "user1"));

            _mockJwtService.Setup(m => m.DecodeJwtToken(It.IsAny<string>()))
              .Returns(UnitTestHelper.CreateAdminToken());

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = fileService.GetByIdWithAuthentification(1, "Bearer + some token");

            //assert
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.IsPublic, actual.IsPublic);
            Assert.AreEqual(expected.UserLogin, actual.UserLogin);
            Assert.AreEqual(expected.DateCreated, actual.DateCreated);
            Assert.AreEqual(expected.FileUrl, actual.FileUrl);
        }

        [Test]
        public void Download_ThrowsException()
        {
            //arrange
            var expectedException = typeof(FileStorageException);
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actualException = Assert.CatchAsync(() => fileService.DownloadAsync(_unexistingFileUrl));

            //assert
            Assert.AreEqual(expectedException, actualException.GetType());
        }

        [Test]
        public void Download_ValidCall()
        {
            //arrange
            var expected = typeof(System.IO.MemoryStream);
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            Task<System.IO.MemoryStream> task = fileService.DownloadAsync(_existingFileUrl);
            var actual = task.Result;

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [TestCase(1)]
        public async Task Update_ValidCall(int id)
        {
            //arrange
            _mockUnitOfWork.Setup(m => m.FileRepository.GetByIdAsync(id))
                .Returns(Task.FromResult(UnitTestHelper.GetTestFileEntities().FirstOrDefault(x => x.Id == id)));

            _mockUnitOfWork.Setup(m => m.FileRepository.Update(It.IsAny<File>()));
            _mockUnitOfWork.Setup(m => m.SaveAsync());

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            await fileService.UpdateAsync(UnitTestHelper.GetTestFileModels().FirstOrDefault(x => x.Id == id));

            //assert
            _mockUnitOfWork.Verify(x => x.FileRepository.Update(It.IsAny<File>()), Times.Exactly(1));
            _mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Exactly(1));
        }

        [Test]
        public void Update_ThrowsArgumentNullException()
        {
            //arrange
            var expected = typeof(ArgumentNullException);
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(() => fileService.UpdateAsync(null));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [TestCase(100)]
        public void Update_ThrowsFileStorageException(int id)
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUnitOfWork.Setup(m => m.FileRepository.GetByIdAsync(id))
               .Returns(Task.FromResult(UnitTestHelper.GetTestFileEntities().FirstOrDefault(x => x.Id == id)));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(() => fileService.UpdateAsync(new FileModel { Id = id }));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [Test]
        public async Task Add_ValidCall()
        {
            //arrange
            _mockUserManager.SetupGet(x=>x.Users)
                .Returns(UnitTestHelper.GetTestUserEntities().AsQueryable());
            _mockUnitOfWork.Setup(m => m.FileRepository.AddAsync(It.IsAny<File>()));
            _mockUnitOfWork.Setup(m => m.SaveAsync());

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            await fileService.AddAsync(UnitTestHelper.GetTestFileModels().First());

            //assert
            _mockUnitOfWork.Verify(x => x.FileRepository.AddAsync(It.IsAny<File>()), Times.Exactly(1));
            _mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Exactly(1));
        }

        [Test]
        public void Add_ThrowsException()
        {
            //arrange
            var expected = typeof(ArgumentNullException);
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(() => fileService.AddAsync(null));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [Test]
        public async Task DeleteById_ValidCall()
        {
            //arrange
            _mockUnitOfWork.Setup(m => m.FileRepository.DeleteByIdAsync(It.IsAny<int>()));
            _mockUnitOfWork.Setup(m => m.SaveAsync());

            _mockUnitOfWork.Setup(m => m.FileRepository.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(UnitTestHelper.GetTestFileEntities().First()));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            await fileService.DeleteByIdAsync(UnitTestHelper.GetTestFileModels().First().Id);

            //assert
            _mockUnitOfWork.Verify(x => x.FileRepository.DeleteByIdAsync(It.IsAny<int>()), Times.Exactly(1));
            _mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Exactly(1));

        }

        [Test]
        public void DeleteById_ThrowsException()
        {
            //arrange
            var expected = typeof(FileStorageException);
            _mockUnitOfWork.Setup(m => m.FileRepository.GetByIdAsync(It.IsAny<int>()));

            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            var actual = Assert.CatchAsync(() => fileService.DeleteByIdAsync(777));

            //assert
            Assert.AreEqual(expected, actual.GetType());
        }

        [Test]
        public void Upload_ValidCall()
        {
            //arrange
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            using var ms = new System.IO.MemoryStream(System.IO.File.ReadAllBytes(_notUploadedFileUrl).ToArray());
            fileService.UploadFile(_sampleFilesUrl, new FormFile(ms, 0, ms.Length, "File", "Proxy.pdf"));
            bool exists = System.IO.File.Exists(_sampleFilesUrl + "/Proxy.pdf");
            System.IO.File.Delete(_sampleFilesUrl + "/Proxy.pdf");

            //assert
            Assert.IsTrue(exists);
        }

        [Test]
        public void Upload_ThrowsException()
        {
            //arrange
            var expectedException = typeof(FileStorageException);
            var fileService = new FileService(_mockUnitOfWork.Object, _mockUserManager.Object, UnitTestHelper.CreateMapperProfile(), _mockJwtService.Object);

            //act
            using var ms = new System.IO.MemoryStream(System.IO.File.ReadAllBytes(_existingFileUrl).ToArray());
            var actual = Assert.Catch(() => fileService.UploadFile(_sampleFilesUrl, new FormFile(ms, 0, ms.Length, "File", "AbstractFactory.pdf")));

            //assert
            Assert.AreEqual(expectedException, actual.GetType());
        }

    }
}