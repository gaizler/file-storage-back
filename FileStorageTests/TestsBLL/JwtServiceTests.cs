﻿using System;
using System.Collections.Generic;
using System.Linq;
using FileStorageBLL.Services;
using FileStorageBLL.Settings;
using FileStorageDAL.Entities;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace FileStorageTests.TestBLL
{
    /// <summary>
    /// unit tests for JwtService
    /// </summary>
    public class JwtServiceTests
    {
        private Mock<IOptionsSnapshot<JwtSettingsModel>> _mockOptionsSnapshot;

        [SetUp]
        public void Setup()
        {
            _mockOptionsSnapshot = new Mock<IOptionsSnapshot<JwtSettingsModel>>();
        }

        [TestCase("username","Admin")]
        public void GenerateJwt_ValidCall(string username, string role)
        {
            //arrange
            var settings = new JwtSettingsModel { 
               ExpirationInDays =10,
               Issuer = "issuer",
               Secret = "superVerySecretKey"
            };

            _mockOptionsSnapshot.Setup(m => m.Value).Returns(settings);
            var jwtService = new JwtService(_mockOptionsSnapshot.Object);
            
            //act
            var tokenStr=jwtService.GenerateJwt(new User{UserName = username,Id = Guid.NewGuid()},new List<string> { role });
            var token = jwtService.DecodeJwtToken(tokenStr);

            //assert
            Assert.AreEqual(token.Claims.First(x => x.Type == "username").Value, username);
            Assert.AreEqual(token.Claims.First(x => x.Type == "roles").Value, role);
        }

        [Test]
        public void DecodeJwt_ValidCall()
        {
            //arrange
            var expectedUsername = "admin";
            var jwtService = new JwtService(_mockOptionsSnapshot.Object);
            
            //act
            var token=jwtService.DecodeJwtToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxZDhiNWY2OS1iZjVkLTRmOWEtNDE4Zi0wOGQ5ZDI4YjRkNTEiLCJ1c2VybmFtZSI6ImFkbWluIiwidXNlcklkIjoiMWQ4YjVmNjktYmY1ZC00ZjlhLTQxOGYtMDhkOWQyOGI0ZDUxIiwianRpIjoiYTEwOTQ0OTktOTVhOS00YjQ4LWEwNDctMmNjZmVjMTk1NzBlIiwibmFtZUlkZW50aWZpZXIiOiIxZDhiNWY2OS1iZjVkLTRmOWEtNDE4Zi0wOGQ5ZDI4YjRkNTEiLCJyb2xlcyI6IkFkbWluIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJleHAiOjE2NDQ0MjMyOTQsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDQzMDAiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjQ0MzAwIn0.zQVUS2P5Zob-fepnR1iUqRAiJIQPWBeN3aMd3nSBx3U");
            var actualUsername = token.Claims.First(x => x.Type == "username").Value;
            
            //assert
            Assert.AreEqual(expectedUsername,actualUsername);
        }

        [Test]
        public void DecodeJwt_ThrowsException()
        {
            //arrange
            var expected = typeof(ArgumentException);
            var jwtService = new JwtService(_mockOptionsSnapshot.Object);

            //act
            var actual=Assert.Catch(()=>jwtService.DecodeJwtToken("Invalid_token"));
            
            //assert
            Assert.AreEqual(expected,actual.GetType());
        }
    }
}
